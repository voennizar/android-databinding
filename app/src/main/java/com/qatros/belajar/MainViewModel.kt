package com.qatros.belajar

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    private val name = MutableLiveData<String>()
    private val lastName = MutableLiveData<String>()
    private val age = MutableLiveData<String>()

    val observableName: LiveData<String> = name
    val observableLastName: LiveData<String> = lastName
    val observableAge: LiveData<String> = age

    fun setName(name: String) {
        this.name.value = name
    }

    fun setLastName(lastName: String) {
        this.lastName.value = lastName
    }

    fun setAge(age: Int) {
        this.age.value = age.toString()
    }

    fun randomName() {
        val newName = listOf("Tinki Winki", "Dipsi", "Lala", "Pow").random()
        Log.d("voen", "Random Name : $newName")
        this.name.postValue(newName)
    }

    fun startActivity() {

    }
}