package com.qatros.belajar

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SecondViewModel : ViewModel() {
    private val counter = MutableLiveData<Int>()

    val observableCounter: LiveData<Int> = counter

    fun increment() {
        val current = counter.value ?: 0
        counter.value = current + 1
    }

    fun resetCounter() {
        counter.value = 0
    }
}