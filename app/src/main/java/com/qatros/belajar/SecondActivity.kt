package com.qatros.belajar

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.qatros.belajar.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivitySecondBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_second)
        val secondViewModel = ViewModelProvider(this).get(SecondViewModel::class.java)

        with(binding) {
            viewModel = secondViewModel
            lifecycleOwner = this@SecondActivity
        }
    }
}