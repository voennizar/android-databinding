package com.qatros.belajar.adapters

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.qatros.belajar.R

@BindingAdapter("setIcon")
fun setIcon(view: View, counter: Int) {
    (view as ImageView).setImageResource(if (counter > 5) R.drawable.ic_android_24dp else R.drawable.ic_child_care)
}
