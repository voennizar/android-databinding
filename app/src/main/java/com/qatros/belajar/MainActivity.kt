package com.qatros.belajar

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.qatros.belajar.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        val viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        binding.viewModel = viewModel

        // need this to enable field observers
        binding.lifecycleOwner = this

        binding.btNewActivity.setOnClickListener {
            startActivity(Intent(this, SecondActivity::class.java))
        }

        with(viewModel) {
            setName(getString(R.string.name_tono))
            setLastName("Moto")
            setAge(50)
        }
    }
}
